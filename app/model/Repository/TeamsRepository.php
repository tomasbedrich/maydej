<?php
/**
 * Teams repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
namespace App\Model;
use Nette\Security\Passwords;

/**
 * Class TeamsRepository
 * @package App\Model
 */
class TeamsRepository extends Repository
{
    public function add($name, $email, $password, $sportId, $leader, $referee, $note) {
        return $this->getAll()
            ->insert(array(
                'name' => $name,
                'email' => $email,
                'password' => Passwords::hash($password),
                'sport_id' => $sportId,
                'leader' => $leader,
                'referee' => $referee,
                'note' => $note
            ));
    }

    public function edit($teamId, $name, $email, $sportId, $leader, $referee, $note) {
        return $this->getAll(array(
            'id' => $teamId
        ))
            ->update(array(
                'name' => $name,
                'email' => $email,
                'sport_id' => $sportId,
                'leader' => $leader,
                'referee' => $referee,
                'note' => $note
            ));
    }

    public function updatePassword($teamId, $password) {
        return $this->getAll(array(
            'id' => $teamId
        ))->update(array(
                'password' => Passwords::hash($password)
            ));
    }

    public function getByEmail($email) {
        return $this->getOne(array(
            'email' => $email
        ));
    }

    public function getBySportId($sportId) {
        return $this->getAll(array(
            'sport_id' => $sportId
        ));
    }

    public function checkPassword($teamId, $password) {
        $row = $this->getOne(array(
            'id' => $teamId
        ));
        return Passwords::verify($password, $row->password);
    }
}
