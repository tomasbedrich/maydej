<?php
/**
 * Update password form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class UpdatePasswordForm
 * @package App\Components
 */
class UpdatePasswordForm extends UI\Control
{
    use TemplateTrait;

    /** @var \App\Model\TeamsRepository */
    private $teams;

    public function __construct(\App\Model\TeamsRepository $teams) {
        $this->teams = $teams;
    }

    protected function createComponentForm() {
        $form = new UI\Form();

        $form->setTranslator($this->presenter->translator);

        $form->addPassword('oldPassword', 'components.updatePasswordForm.oldPassword.title')
            ->setRequired('components.updatePasswordForm.oldPassword.required');

        $form->addPassword('password', 'components.updatePasswordForm.password.title')
            ->setRequired('components.updatePasswordForm.password.required');

        $form->addPassword('password2', 'components.updatePasswordForm.password2.title')
            ->setRequired('components.updatePasswordForm.password2.required')
            ->addRule(UI\Form::EQUAL, 'components.updatePasswordForm.password2.notEqual', $form['password']);

        $form->addSubmit('submit', 'components.updatePasswordForm.submit');

        $form->onValidate[] = $this->validateForm;
        $form->onSuccess[] = $this->processForm;

        return $form;
    }

    public function validateForm(UI\Form $form) {
        $values = $form->getValues();

        if(!$this->teams->checkPassword($this->presenter->user->getId(), $values->oldPassword)) {
            $form->addError('components.updatePasswordForm.oldPassword.notEqual');
        }
    }

    public function processForm(UI\Form $form) {
        $values = $form->getValues();
        
        $this->teams->updatePassword($this->presenter->user->getId(), $values->password);
        $this->presenter->flashMessage('components.updatePasswordForm.flashes.success');
        $this->presenter->redirect('this');
    }
}

/**
 * Interface IUpdatePasswordFormFactory
 * @package App\Components
 */
interface IUpdatePasswordFormFactory
{
    /**
     * Creates update password form component
     * @return \App\Components\UpdatePasswordForm
     */
    function create();
}
